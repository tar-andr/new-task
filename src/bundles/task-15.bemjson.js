/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-15 Умный фильтр',
  content: [
      {
          block: 'filter',
          content: [
              {
                  elem: 'header',
                  content: 'Поиск по характеристикам',
              },
              {
                  elem: 'body',
                  content: [
                      {
                          elem: 'price-wrap',
                          mix: { block: 'form-group'},
                          cls: ' clearfix',
                          content: [
                              {
                                  elem:'title',
                                  content: 'Цена товара (p)',
                              },
                              {
                                  block: 'range',
                                  content: [
                                      {
                                          elem: 'slider',
                                      },
                                      {
                                          elem: 'input',
                                          mix: {block: 'form-control' },
                                          elemMods: {limit: 'min'}, //min,max},
                                          attrs: {
                                              value: '0',
                                          },
                                      },
                                      {
                                          elem: 'input',
                                          mix: {block: 'form-control' },
                                          elemMods: {limit: 'max'}, //min,max},
                                          attrs: {
                                              value: '4000',
                                          },
                                      },
                                  ],
                              },

                          ],
                      },
                      {
                          elem: 'type-wrap',
                          mix: {block: 'form-group'},
                          content: [
                              {
                                  elem: 'title',
                                  content: 'Тип',
                              },
                              {
                                  elem: 'types',
                                  content: ['Маска сварщика'].map((type)=>[
                                      {
                                          elem: 'check-group',
                                          mix: {block: 'form-check'},
                                          content: [
                                              {
                                                  elem: 'check-input',
                                                  mix: {block: 'form-check-input'},
                                                  attrs: {
                                                      value: 'mask',
                                                  },
                                              },
                                              {
                                                  elem: 'label',
                                                  mix: {block: 'form-check-label'},
                                                  content: type,
                                              },
                                          ],
                                      },
                                  ]),
                              },
                          ],
                      },
                      {
                          elem: 'brand-wrap',
                          mix: {block: 'form-group'},
                          content: [
                              {
                                  elem: 'title',
                                  content: 'Брэнд',
                              },
                              {
                                  elem: 'brand',
                                  content: ['AURORA', 'PIT', 'РЕСАНТА', 'СИБРТЕХ'].map((brand, index)=>[
                                      {
                                          elem: 'check-group',
                                          mix: {block: 'form-check'},
                                          content: [
                                              {
                                                  elem: 'check-input',
                                                  mix: {block: 'form-check-input'},
                                                  attrs: {
                                                      value: brand,
                                                  },
                                              },
                                              {
                                                  elem: 'label',
                                                  mix: {block: 'form-check-label'},
                                                  content: brand,
                                              },
                                          ],
                                      },
                                  ]),
                              },
                          ],
                      },
                      {
                          elem: 'select-wrap',
                          mix: {block: 'form-group'},
                          content: [
                              {
                                  elem: 'title',
                                  content: 'Цвет',
                              },
                              {
                                  elem: 'select',
                                  content: ['blue', 'red', 'green', 'yellow', 'pink', 'black', 'white'].map((color)=>[
                                      {
                                          elem: 'option',
                                          attrs: {
                                              value: color,
                                          },
                                          content: color,
                                      },
                                  ]),
                              },
                          ],
                      },
                      {
                          elem: 'footer',
                          content:{
                              block: 'btn',
                              tag: 'input',
                              content: 'Найти',
                          },
                      },
                  ],
              },
          ],
      },
  ],
};
