module.exports = {
  block: 'page',
  title: 'task-2 todo форма',
  content: [
    {
      block: 'container', content: {
        block: 'form-auth',
        mods: {type: 'position'}, // flex,position,table,inline
        content: {
            elem: 'wrapper',
            cls: 'w-75',
            content: [
                {
                    block: 'form-group',
                    content: {
                        block: 'form-control',
                        placeholder: 'Email',
                        attrs: {
                            type: 'email',
                        },
                    },
                },
                {
                    block: 'form-group',
                    content: {
                        block: 'form-control',
                        placeholder: 'Password',
                        attrs: {
                            type: 'password',
                        },
                    },
                },
                {
                    block: 'btn',
                    tag: 'input',
                    cls: 'btn-primary',
                    content: 'Go in',
                },
            ],
        },
      },
    },
  ],
};
