/* eslint-disable max-len */
module.exports = [
  {block: 'header',
      content: {
          block: 'container',
          content: {
              block: 'nav',
              tag: 'nav',
              content: {
                  elem: 'list',
                  elemMods: {type: 'flex'}, //flex|table
                  tag: 'ul',
                  content: ['Пункт 1', 'Пункт 1', 'Пункт 1', 'Пункт 1', 'Пункт 1', 'Пункт 1', 'Пункт 1'].map((item)=>[
                      {
                          elem: 'item',
                          tag: 'li',
                          content: item,
                      },
                  ]),
              },
          },
      },
  },
];
