/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-9 Таблица с фиксированной шапкой',
  content: {
      block: 'wrapper',
      content: ['head','body'].map((table)=>[
          {
              elem:'table',
              content: {
                  block: 'table-fix',
                  mods: {position: table}, //head|body
                  content: [
                      {
                          elem: 'head',
                          tag: 'thead',
                          content: {
                              elem: 'tr',
                              tag: 'tr',
                              content: ['Make', 'Model', 'Color', 'Year'].map((th)=>[
                                  {
                                      elem: 'th',
                                      tag: 'th',
                                      content: th,
                                  },
                              ]),
                          },

                      },
                      {
                          elem: 'body',
                          tag: 'tbody',
                          content: new Array(6).fill('').map((tr, indtr)=>[
                              {
                                  elem: 'tr',
                                  tag: 'tr',
                                  content: ['Ford', 'Escort', 'Blue', '2000'].map((td)=>[
                                      {
                                          elem: 'td',
                                          tag: 'td',
                                          content: td,
                                      },
                                  ]),
                              },
                          ]),

                      },
                  ],
              },
          },
      ]),

  },
};
