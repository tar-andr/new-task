/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-11 Ярлык к плитке',
  content: [
      {
          block: 'grid',
          mods: {type: 'flex'}, // flex|inline|equal-height
          content: [
              require('../blocks.04-project/product/product.tmpl-specs/01.product-default.bemjson'),
              require('../blocks.04-project/product/product.tmpl-specs/02.product-sale.bemjson')
          ],
      },
  ],
};
