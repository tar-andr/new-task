/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-12 Нестандартное поле ввода',
  content: [
      {
          block: 'input-custom',
          content: [
              {
                  elem: 'input',
              },
              {
                  elem: 'label',
                  content: {
                      elem: 'placeholder',
                      content: 'First Name',
                  },
              },
          ],
      },
  ],
};
