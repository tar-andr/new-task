/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-8 Появляющийся список',
  content: [
      {
          block: 'search',
          content: {
              elem: 'place',
              cls: 'w-100',
              content: [
                  {
                      elem: 'check',
                  },
                  {
                      elem: 'icon',
                      mix: {block: 'fa'},
                  },
                  {
                      elem: 'input',
                  },
              ],
          },
      },
  ],
};
