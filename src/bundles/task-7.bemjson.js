/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-7 Прозрачная граница',
  content: [
      {
          block: 'contacts',
          cls: 'w-90',
          content: [
              {
                  elem: 'header',
                  content: [
                      {
                          elem: 'border',
                      },
                      {
                          elem: 'title',
                          tag: 'h2',
                          content: 'contact us',
                      },
                      {
                          elem: 'border',
                      },
                  ],
              },
              {
                  elem: 'descr',
                  cls: 'w-75',
                  tag: 'p',
                  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
              },
              {
                  elem: 'button',
                  cls: 'w-50',
                  content: 'contact us',
              },
          ],
      },
  ],
};
