module.exports = {
  block: 'page',
  title: 'Плитка товаров',
  content: [
    {
      block: 'grid',
      mods: {type: 'flex'}, // flex|inline|equal-height
        content: new Array(7).fill('').map((item, index)=>[
            {
                elem: 'col',
                    content: {
                        block: 'product',
                        content: [
                            {
                                block: 'img', src: `http://placehold.it/${Math.floor(Math.random()*100)}x${Math.floor(Math.random()*100)}`, content: 'my image',
                                mix: {elem: 'img', block: 'product'},
                            },
                            {
                                elem: 'text',
                                content: 'Тут описание товара...',
                            },
                            {
                                elem: 'rating',
                                tag: 'form',
                                attrs: {
                                    'method': 'POST',
                                    'action': '#',
                                },
                                content: [
                                    {
                                        elem: 'wrap',
                                        cls: 'clearfix',
                                        content: [5, 4, 3, 2, 1].map((star) => [
                                            {
                                                elem: 'input',
                                                tag: 'input',
                                                attrs: {
                                                    type: 'radio',
                                                    value: star,
                                                    required: 'true',
                                                },
                                            },
                                            {
                                                elem: 'icon',
                                                mix: {block: 'fa'},
                                                tag: 'label',
                                                cls: 'fa-star-o fa-lg',
                                            },
                                        ]),

                                    },
                                    {
                                        block: 'btn',
                                        tag: 'input',
                                        content: 'Отправить',
                                    },
                                ],
                            },
                        ],
                    },

            },
        ]),
    },
  ],
};
