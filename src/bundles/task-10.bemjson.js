/* eslint-disable max-len,no-trailing-spaces */
module.exports = {
  block: 'page',
  title: 'task-10 Ромбовая картинка',
  content: [
      {
          block: 'rhombus', content: {
              block: 'img', src: 'http://placehold.it/300x300', content: 'my image',
              mix: {elem: 'img', block: 'rhombus'},
          },
      },
  ],
};
