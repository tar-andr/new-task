/* eslint-disable max-len */
module.exports = {
  block: 'page',
  title: 'task-5,6 Обрезание текста ',
  content: [
      {
          block: 'container',
          content: [
              {
              block: 'row',
              cls: 'justify-content-between',
              content: new Array(3).fill('').map((item1, index1)=>[
                  {
                      block: 'product',
                      mods: {type: 'dot'}, //dot, fade
                      cls: 'col-3',
                      attrs: {
                          'data-id-product': index1+1,
                          'data-height': 80,
                      },
                      content: [
                          {
                              elem: 'title',
                              content: 'Просто очень много много много много текста Просто очень много много много много текста',
                          },
                          {
                              block: 'img', src: 'http://placehold.it/200x100', content: 'my image',
                              mix: {elem: 'img', block: 'product'},
                          },
                          {
                              elem: 'text',
                              content: 'Какое-то описание товара',
                          },
                      ],
                  },
              ]),
              },
              {
                  block: 'row',
                  cls: 'justify-content-between',
                  content: new Array(3).fill('').map((item1, index)=>[
                      {
                          block: 'product',
                          mods: {type: 'fade'}, //dot,fade
                          cls: 'col-3',
                          attrs: {
                              'data-id-product': index+1,
                              'data-height': 80,
                          },
                          content: [
                              {
                                  elem: 'title',
                                  content: 'Просто очень много много много много текста Просто очень много много много много текста Просто очень много много много много текста',
                              },
                              {
                                  block: 'img', src: 'http://placehold.it/200x100', content: 'my image',
                                  mix: {elem: 'img', block: 'product'},
                              },
                              {
                                  elem: 'text',
                                  content: 'Какое-то описание товара',
                              },
                          ],
                      },
                  ]),
              },
          ],
      },
  ],
};
