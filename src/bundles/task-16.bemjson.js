module.exports = {
  block: 'page',
  title: 'Зависимый фильтр',
  content: [
      {block: 'select-car', content: [
          {block: 'form-control', tag: 'select', attrs: {  'name': 'MARK', 'data-target': '[data-control="MODEL"]'}, content: [
              'Не выбранно',
              ...['BMW', 'Audi', 'Lexus'].map((model)=> [
                  {
                      tag:'option',
                      attrs: {
                          value: model,
                      },
                      content: model,
                  },
              ]),
          ]},
          {block: 'form-control', attrs: {'name': 'MODEL', 'disabled': true, 'data-control':'MODEL', 'data-target': '[data-control="YEAR"]'}, tag: 'select', content: [
              'Не выбранно',
          ]},
          {block: 'form-control', attrs: {'name': 'YEAR', 'disabled': true, 'data-control':'YEAR'}, tag: 'select', content: [
              'Не выбранно'

          ]},
      ]},
  ],
};
