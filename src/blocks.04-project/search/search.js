/**
 * Created by 1 on 23.10.2018.
 */
const Search = {
    'BLOCK': 'search',
    'FORM': 'search__place',
    'ICON': 'search__icon',
    'INPUT': 'search__input',
};
const Events = {
    FOCUS: 'focus',
    BLUR: 'blur',
};

(($)=>{
    $(`.${Search.FORM}`).on('click', `.${Search.ICON}`, (event) => {
        let $input = $(event.delegateTarget).find(`.${Search.INPUT}`);
        if (event.delegateTarget.classList.toggle('open')) {
            $input.trigger(Events.FOCUS);
        } else {
            $input.trigger(Events.BLUR);
        }
    });
})($);