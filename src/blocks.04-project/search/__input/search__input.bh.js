module.exports = (bh) => {
    bh.match('search__input', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'text',
            name: ctx.tParam('ID'),
        });
    });
};
