module.exports = (bh) => {
    bh.match('search__icon', (ctx, json) => {
        ctx.tag('label').attrs({
            for: ctx.tParam('ID'),
        });
    });
};
