module.exports = (bh) => {
    bh.match('search__check', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'checkbox',
            id: ctx.tParam('ID'),
        });
    });
};
