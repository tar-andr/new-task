/**
 * Created by 1 on 24.10.2018.
 */

import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widget';
import 'jquery-ui/ui/widgets/mouse';
import 'jquery-ui/ui/widgets/slider';

const RANGE = {
    'BLOCK': 'range',
    'SLIDER': 'range__slider',
    'MAX': 'range__input_limit_max',
    'MIN': 'range__input_limit_min',
};

(($)=>{
    let $min = $(`.${RANGE.MIN}`);
    let $max = $(`.${RANGE.MAX}`);
    let $slider = $(`.${RANGE.SLIDER}`);
    $slider.slider({
        range: true,
        min: Number($min.val()),
        max: Number($max.val()),
        values: [Number($min.val()), Number($max.val())],
        stop: (event, ui)=>{
            $min.val(ui.values[0]);
            $max.val(ui.values[1]);
        },
        slide: (event, ui)=>{
            $min.val(ui.values[0]);
            $max.val(ui.values[1]);
        },
    });


    $min.change(function () {
        let $val1 = Number($min.val());
        $slider.slider('values',0,$val1);
    });
    $max.change(function () {
        let $val2 = Number($max.val());
        $slider.slider('values',1,$val2);
    });

})($);