module.exports = (bh) => {
    bh.match('range__input', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'number',
            name: ctx.tParam('NAME'),
            min: 0,
            max: 4000,
        });
    });
};
