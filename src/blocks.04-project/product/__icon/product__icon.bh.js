module.exports = (bh) => {
    bh.match('product__icon', (ctx, json) => {
        ctx.tag('input').attrs({
            for: ctx.tParam('ID') + '_' + (ctx.position() - 1),
        });
    });
};
