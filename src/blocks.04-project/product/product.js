import 'jquery.dotdotdot';
const TitleType = {
    'BLOCK': 'product',
    'TITLE': 'product__title',
    'DOT': 'product_type_dot',
    'FADE': 'product_type_fade',
};
const $height = $(`.${TitleType.BLOCK}`).data('height');

(($)=>{
    console.log($height);
    $(`.${TitleType.DOT}`)
        .find(`.${TitleType.TITLE}`)
        .dotdotdot({
            ellipsis: '...',
            wrap: 'letter',
            fallbackToLetter: true,
            after: null,
            watch: true,
            height: 70,
            tolerance: 0,
            callback: function(isTruncated, orgContent) {},
        });
    $(`.${TitleType.FADE}`)
        .find(`.${TitleType.TITLE}`)
        .dotdotdot({
            wrap: 'letter',
            watch: true,
            height: $height || 90,
        });
})($);