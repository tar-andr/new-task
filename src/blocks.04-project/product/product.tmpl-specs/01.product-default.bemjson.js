module.exports = [
    {
        block: 'product',
        mix: {elem: 'col', block: 'grid'},
        content: [
            {
                elem: 'title',
                content: 'Название',
            },
            {
                block: 'img', src: 'http://placehold.it/200x100', content: 'my image',
                mix: {elem: 'img', block: 'product'},
            },
            {
                elem: 'text',
                content: 'Какое-то описание товара, Какое-то описание товара, Какое-то описание товара, Какое-то описание товара',
            },
        ],
    },
];

