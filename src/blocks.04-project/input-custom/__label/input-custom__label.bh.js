module.exports = (bh) => {
    bh.match('input-custom__label', (ctx, json) => {
        ctx.tag('label').attrs({
            for: ctx.tParam('ID'),
        });
    });
};
