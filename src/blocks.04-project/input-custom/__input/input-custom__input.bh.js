module.exports = (bh) => {
    bh.match('input-custom__input', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'text',
            name: ctx.tParam('ID'),
            id: ctx.tParam('ID'),
        });
    });
};
