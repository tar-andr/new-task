module.exports = (bh) => {
    bh.match('filter__label', (ctx, json) => {
        ctx.tag('label').attrs({
            'for': ctx.tParam('ID'),
        });
    });
};
