module.exports = (bh) => {
    bh.match('filter__select', (ctx, json) => {
        ctx.tag('select').attrs({
            multiple: true,
        });
    });
};
