/**
 * Created by 1 on 24.10.2018.
 */

import 'chosen-js/chosen.jquery.min';

const Selectors = {
    'BLOCK': '.filter',
    'SELECT': '.filter__select',
};

(($)=>{
    $(Selectors.SELECT).chosen({
        width: '100%',
        disable_search: false,
        no_result_text: 'Ничего не найдено',
        placeholder_text_multiple: 'Выбирете несколько параметров',
        display_disabled_options: false,
        display_selected_options: false,
    });

})($);