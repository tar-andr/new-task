module.exports = (bh) => {
    bh.match('filter__check-input', (ctx, json) => {
        ctx.tag('input').attrs({
            type: 'checkbox',
            name: ctx.tParam('ID'),
            id: ctx.tParam('ID'),
        });
    });
};
