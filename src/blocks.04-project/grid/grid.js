import 'jquery.equalheights';
const ClassName = {
    'BLOCK': 'grid',
    'COL': 'grid__col',
    'EQ_HEIGHT': 'grid_type_equal-height',
};

(($)=>{
        $(`.${ClassName.EQ_HEIGHT}`)
            .find(`.${ClassName.COL}`)
            .equalHeights({watch: true, wait: true});

})($);

