// const data = {
//     MARK: {
//         'BMW': ['X3', 'X5', 'X6'],
//         'Audi': ['Q3', 'Q5', 'Q7'],
//     },
//     MODEL: {
//         'X3': [
//             '2006',
//             '2010',
//             '2014',
//         ],
//         'X5': [
//             '2006',
//             '2010',
//             '2013',
//         ],
//         'X6': [
//             '2007',
//             '2012',
//             '2014',
//         ],
//         'Q3': [
//             '2011',
//             '2014',
//             '2017',
//         ],
//         'Q5': [
//             '2008',
//             '2012',
//             '2017',
//         ],
//         'Q7': [
//             '2005',
//             '2009',
//             '2015',
//         ],
//     },
// };

const ClassName = {
    'BLOCK': 'select-car',
    'MARK': 'select-car__mark',
    'MODEL': 'select-car__model',
    'YEAR': 'select-car_year',
};
// $.ajax(`stub/${MARK|MODEL}.json`, {
//
// })

(($)=>{

       $(`.${ClassName.BLOCK}`).on('change', $('select'), function (event) {
           let $select = $(event.target);
           let $target = $($select.data('target'));
           let name = $select.attr('name');
           let value = $select.val();
            // MARK|MODEL
           if(name == 'YEAR') return;
           $.ajax(`stub/${name}.json`, {
               type: 'GET',
               dataType: 'json',
               success: function (data) {
                   $target
                       .find('option:not(:first-child)')
                       .remove();
                   $target
                       .prop('disabled', true);
                   if (Array.isArray(data[value])) {
                       $target
                           .prop('disabled', false)
                           .append(data[value].map((value) => {
                               let option = document.createElement('option');
                               option.value = value;
                               option.innerHTML = value;
                               return option;
                           })).trigger('change');
                   }
               },
               error: function (error) {
                 console.warn(error);
               },
           });
           /*if(data[name]){
               $target
                   .find('option:not(:first-child)')
                   .remove();
               $target
                   .prop('disabled', true);
               if (Array.isArray(data[name][value])) {
                   $target
                       .prop('disabled', false)
                       .append(data[name][value].map((value) => {
                           let option = document.createElement('option');
                           option.value = value;
                           option.innerHTML = value;
                           return option;
                       })).trigger('change');
               }
           }*/


           // Old
           // let option = $('option:selected', $select).val();
           // let dataTarget = $select.attr('data-target');


           // let res = [];
           // $(dataTarget).prop('disabled',false).empty();

           // option
           // $select.name
           // let name = 'MARK' | 'MODEL';
           // let key = 'Q7 | A6'
           // Object.keys(cars).forEach((mark)=>{
           //     Object.keys(cars[mark]).forEach((model)=>{
           //         res = cars[mark][model];
           //     });
           // });
           // for(let mark in cars){
           //     for(let model in cars[mark]){
           //         if(option == mark){
           //             res.push(model);
           //         }else if(option == model){
           //             cars[mark][model].map((year)=>{
           //                 res.push(year);
           //             });
           //         }
           //     }
           // }
           // for(let i = 0; i <= res.length - 1; i++){
           //     $select.next('select').append('<option>' + res[i] + '</option>')
           // }
       }) ;

})($);